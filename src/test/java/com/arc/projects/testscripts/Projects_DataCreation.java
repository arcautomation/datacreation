package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.Common.utils.Generate_Random_Number;
import com.arc.Common.utils.PropertyReader;
import com.arc.marketPlace.pages.FolderPage;
import com.arc.marketPlace.pages.ProjectDashboardPage;
import com.arc.marketPlace.pages.ProjectsLoginPage;

import com.arcautoframe.utils.*;


@Listeners(EmailReport.class)

public class Projects_DataCreation {
	
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
   
    ProjectDashboardPage projects_ExportToCSVScenarios;
    
    @Parameters("browser")
    @BeforeMethod
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));      
       
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
     
    
      @Test(priority = 0, enabled = true, description = "Project Test Data Creation")
      public void verify_CreateProject() throws Exception
      {
      	try
      	{
      		Log.testCaseInfo("Project Data Creation");      				
      	    //Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username1");
    		String pWord = PropertyReader.getProperty("Password1");
    		
    		//=====Login with valid UserID/PassWord
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);
      		
      		String Count = PropertyReader.getProperty("NumberOfprojectCount");
      		
      		for(int i=0; i<=Integer.parseInt(Count.trim());i++ ){
      			
      		//=========Create Project Randomly===================================
      		String Project_Name = "Project_"+ Generate_Random_Number.generateRandomValue();
      		projectDashboardPage.createProject(Project_Name);
      		Log.message("Create project ####"+i ); 
      		Log.message("Create project name:"+Project_Name); 
      		
      		}
      		
      	}
          catch(Exception e)
          {
          	e.getCause();
          	Log.exception(e, driver);
          }
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }
    
      
     
        
}
