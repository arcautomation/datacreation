package com.arc.Common.utils;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class CommonMethod {
	
	static WebDriver driver;
	//Alert alert = null;
	
	
	
	
	
	public static  boolean selectByValue(WebElement element, String value) {
		boolean flag = false;
		try {
			//WebElement element=(WebElement) driver.findElements(state_INBOX2);
			Select se=new Select(element);
			se.selectByVisibleText(value);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} 
		}
	
	public void DragAndDrop(WebElement toDrag ,WebElement toDrop){
        try{
             Actions builder = new Actions(driver);
                builder.keyDown(Keys.CONTROL)
                    .click(toDrag)
                    .dragAndDrop(toDrag, toDrop)
                    .keyUp(Keys.CONTROL);
     
                    Action selected = builder.build();
                    selected.perform();
        }catch(Exception e){
            e.printStackTrace();
        }
        
	}
        
        public static void waitForPageLoaded() throws HeadlessException, AWTException,
		IOException {
	ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			return ((JavascriptExecutor) driver)
					.executeScript("return document.readyState").toString()
					.equals("complete");
		}
	};
	try {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
//		wait.until(expectation);
	} catch (Throwable error) {
		// takeScreenShotOnError(error.toString());
		Assert.fail("Timeout waiting for Page Load Request to complete.");
	}
}
	
        public static boolean isElementPresent(By by)
    			throws Throwable {
    		boolean flag = false;
    		try {
    			driver.findElement(by);
    			flag = true;
    			return true;
    		} catch (Exception e) {
    			
    			e.printStackTrace();
    			return false;
    		}       
        
        }
        
        
        public static boolean js_type(WebElement element, String Text)
    			throws Throwable {
    		boolean flag = true;
    		try {
    			
    			((JavascriptExecutor) driver).executeScript("arguments[0].value='"
    					+ Text + "'", element);
    			return true;
    		} catch (Exception e) {
    			flag = false;
    			return false;
    		}
    		}
        
        /**
    	 * Moves the mouse to the middle of the element. The element is scrolled
    	 * into view and its location is calculated using getBoundingClientRect.
    	 * 
    	 * @param locator
    	 *            : Action to be performed on element (Get it from Object
    	 *            repository)
    	 * 
    	 */    
        
        public static boolean mouseover(WebElement element)
    			throws Throwable {
    		boolean flag = false;
    		try {
    			//WebElement mo = driver.findElement(locator);
    			new Actions(driver).moveToElement(element).build().perform();
    			flag = true;
    			return true;
    		} catch (Exception e) {
    			Assert.assertTrue(flag, "MouseOver action is not perform on ");
    			return false;
    		}
    		
        }
        
        //=================Function for Drag And Drop===============================
        
        /**
    	 * A convenience method that performs click-and-hold at the location of the
    	 * source element, moves by a given offset, then releases the mouse.
    	 * 
    	 * @param source
    	 *            : Element to emulate button down at.
    	 * 
    	 * @param xOffset
    	 *            : Horizontal move offset.
    	 * 
    	 * @param yOffset
    	 *            : Vertical move offset.
    	 * 
    	 */
        
        public static boolean draggable(WebElement source , int x, int y)
    			throws Throwable {
    		boolean flag = false;
    		try {
    			
    			new Actions(driver).dragAndDropBy(source, x, y).build().perform();
    			Thread.sleep(5000);
    			flag = true;
    			return true;

    		} catch (Exception e) {

    			return false;
    		}}
        
        // ===========create Function for scrollBar=====================
        /**
    	 * To slide an object to some distance
    	 * 
    	 * @param slider
    	 *            : Action to be performed on element
    	 */
        public static boolean slider(WebElement slider, int x, int y)
    			throws Throwable {

    		boolean flag = false;
    		try {
    			//WebElement dragitem = driver.findElement(slider);
    			// new Actions(driver).dragAndDropBy(dragitem, 400, 1).build()
    			// .perform();
    			new Actions(driver).dragAndDropBy(slider, x, y).build().perform();// 150,0
    			Thread.sleep(5000);
    			flag = true;
    			return true;
    		} catch (Exception e) {

    			return false;
    		}}
        
        
        public static boolean rightclick(WebElement elementToRightClick, String locatorName)
    			throws Throwable {

    		boolean flag = false;
    		try {
    			Actions clicker = new Actions(driver);
    			clicker.contextClick(elementToRightClick).perform();
    			flag = true;
    			return true;
    			
    		} catch (Exception e) {

    			return false;
    		}}
        
        
        
        /**
    	 * This method returns check existence of element
    	 * 
    	 * @param locator
    	 *            : Action to be performed on element (Get it from Object
    	 *            repository)
    	 * @param locatorName
    	 *            : Meaningful name to the element (Ex:Textbox, checkbox etc)
    	 * @return: Boolean value(True or False)
    	 * @throws NoSuchElementException
    	 */
    	public static boolean isElementPresent(WebElement element)
    			throws Throwable {
    		boolean flag = false;
    		try {
    			//driver.findElement(by);
    			flag = true;
    			return true;
    		} catch (Exception e) {
    			
    			e.printStackTrace();
    			return false;
    		} }
        
        
       
        
    	/**
    	 * @return: return title of current page.
    	 * 
    	 * @throws Throwable
    	 */

    	/*public static String getTitle() throws Throwable {

    		String text = driver.getTitle();
    		if (text) {
    			
    			Log.message("GetTitle: " + b + " " + "Title Present On the Text" );
    			
    		}
    		return text;
    	}*/
    	
    	public static boolean refreshPage() throws Throwable {
    		boolean flag = false;
    		try {
    			driver.navigate().refresh();
    			flag = true;
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
			return flag;}
    	
    	
    	
    	public static boolean isElementDisplayed(WebElement element)
    			throws Throwable {
    		boolean flag = false;
    		try {
    			for (int i = 0; i < 200; i++) {
    				if (element.isDisplayed()) {
    					flag = true;
    					break;
    				} else {
    					Thread.sleep(50);
    				}
    			}
    		} catch (Exception e) {
    			return false;
    		}
    		return flag;
    	}
        
    	public static void waitForJSandJQueryToLoad() {

    		Boolean isJqueryUsed = (Boolean) ((JavascriptExecutor) driver)
    				.executeScript("return (typeof(jQuery) != 'undefined')");
    		if (isJqueryUsed) {
    			while (true) {
    				// JavaScript test to verify jQuery is active or not
    				Boolean ajaxIsComplete = (Boolean) (((JavascriptExecutor) driver)
    						.executeScript("return jQuery.active == 0"));
    				if (ajaxIsComplete)
    					break;
    				try {
    					Thread.sleep(100);
    				} catch (InterruptedException e) {
    				}
    			}
    		}
    	}
    	
    	
    	public static boolean waitForInVisibilityOfElement(By by, String locator)
    			throws Throwable {
    		boolean flag = false;
    		WebDriverWait wait = new WebDriverWait(driver, 30);
    		try {
//    			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    			flag = true;
    			return true;
    		} catch (Exception e) {
    			return false;
    		} }
}